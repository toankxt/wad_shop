<?php 
    function addProduct($db){
        if(isset($_POST['them'])){
            $name = $_POST['name'];
            $category = $_POST['category'];
            $quantity = $_POST['quantity'];
            $amount = $_POST['amount'];
            $description = $_POST['description'];
            if(isset($_FILES['image'])){
                $image = explode('.',$_FILES['image']['name']);
                $image='assets/uploads/'.$image[0].'-'.time().'.'.$image[(count($image)-1)];
                $image_tmp=$_FILES['image']['tmp_name'];
                move_uploaded_file($image_tmp,$image);
            }else{
                $image = 'assets/images/img_default.png';
            }
            $table = "product";
            $column ="`category`,`name`,`image`,`quantity`,`amount`,`description`";
            $value = "'$category','$name','$image','$quantity','$amount','$description'";
            $result = $db->insertData($table,$column,$value);
            if($result == FALSE || $result == 0){
                echo 'up lỗi'; // trường hợp lỗi
            }else{
                $themthanhcong[]  = 'add_success';
            }
        }
    }
    /*  $sql = "update $table set name = '$name',category = '$category',quantity = '$quantity',amount = '$amount',description='$description',image = '$image' where '$id'";*/
        /*  $sql = "update $table set name = '$name',category = '$category',quantity = '$quantity',amount = '$amount',description='$description',image = '$image' where '$id'";*/
    function editProduct($db){
        if (isset($_POST['sua'])) {
            $name = $_POST['name'];
            $category = $_POST['category'];
            $quantity = $_POST['quantity'];
            $amount = $_POST['amount'];
            $description = $_POST['description'];
           if(isset($_FILES['image'])){
                $image = explode('.',$_FILES['image']['name']);
                $image='assets/uploads/'.$image[0].'-'.time().'.'.$image[(count($image)-1)];
                $image_tmp=$_FILES['image']['tmp_name'];
                move_uploaded_file($image_tmp,$image);
            }
            $id = "`id`=".$_GET['id'];
            $table = "product";
            $value = "`category`='$category',`name`='$name',`image`='$image',`quantity`='$quantity',`amount`='$amount',`description`='$description'";
            //$column ="`category`,`name`,`image`,`quantity`,`amount`,`description`";
            $result = $db->updateData($table,$id,$value);
            if($result == FALSE || $result == 0){
                $error="Sửa thất bại";
            }else{
                header('location:?controller=admin&action=quan-ly-san-pham');
            }
        }
    }
    function addOrder($db){
        if(isset($_POST['them_Oder'])){
            $name = $_POST['name'];
            $category = $_POST['category'];
            $quantity = $_POST['quantity'];
            $amount = $_POST['amount'];
            $description = $_POST['description'];
            if(isset($_FILES['image'])){
                $image = explode('.',$_FILES['image']['name']);
                $image='assets/uploads/'.$image[0].'-'.time().'.'.$image[(count($image)-1)];
                $image_tmp=$_FILES['image']['tmp_name'];
                move_uploaded_file($image_tmp,$image);
            }else{
                $image = 'assets/images/img_default.png';
            }
            $table = "product";
            $column ="`category`,`name`,`image`,`quantity`,`amount`,`description`";
            $value = "'$category','$name','$image','$quantity','$amount','$description'";
            $result = $db->insertData($table,$column,$value);
            if($result == FALSE || $result == 0){
                echo 'up lỗi'; // trường hợp lỗi
            }else{
                $themthanhcong[]  = 'add_success';
            }
        }
    }
    function editUser($db){
        if (isset($_POST['suauser'])) {
            print_r($_POST);
            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $gender = $_POST['gender'];
            $level = $_POST['level'];
            $id = "`id`=".$_GET['id'];
            $table = "user";
            $value = "`fullname`='$fullname',`address`='$address',`gender`=$gender,`level`='$level'";
            $result = $db->updateData($table,$id,$value);
            if($result == FALSE || $result == 0){
                $error="Sửa thất bại";
            }else{
                header('location:?controller=admin&action=quan-ly-users');
            }
        }
    }
?>

<?php
    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }else{
        $action = '';
    }
    switch($action){
        case 'index':{
            $table = "category";
            $column ='name';
            if(isset($_POST['them'])){
                $value = "'$_POST[$column]'";
                 $themthanhcong = array();
                if($db->insertData($table,$column,$value)){
                    $themthanhcong[]  = 'add_success';
                }
            }
            $data = $db->getAllData($table);
            require_once('Views/admin/ThemDanhMuc/index.php');
            break;
        }
        case 'them-san-pham':{
            $table = "category";
            $data_loai = $db->getAllData($table);
            addProduct($db);
            require_once('Views/admin/ThemSanPham/index.php');
            break;
        }
        case 'sua-san-pham':{
            $tableCategory = "category";
            $tableProdct = "product";
            $data_loai = $db->getAllData($tableCategory);
            $id =  "`id`=".$_GET['id'];
            //goi du lieu ra
            $infoProduct = $db->getDataByOption($tableProdct,$id); //lấy dữ liệu ra theo ID
            editProduct($db);
            require_once('Views/admin/SuaSanPham/index.php');
            break;
        }
        case 'quan-ly-san-pham':{
            $table = "product";
            $data = $db->getAllData($table);
        //phân trang
         //BƯỚC 1 : KẾT NỐI 
             $tenmaychu='localhost';
             $tentaikhoan='root';
             $pass='';
             $csdl='wad_shop';
             $conn=mysqli_connect($tenmaychu, $tentaikhoan, $pass, $csdl);
        // BƯỚC 2: TÌM TỔNG SỐ RECORDS
             $tongsp= $db->count_document($table);
        // BƯỚC 3: TÌM LIMIT VÀ CURRENT_PAGE
            $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
            $limit = 4;
        // BƯỚC 4: TÍNH TOÁN TOTAL_PAGE VÀ START
        // tổng số trang
            $tongsotrang = ceil($tongsp / $limit);
        // Giới hạn current_page trong khoảng 1 đến total_page
        if ($current_page > $tongsotrang){
            $current_page = $tongsotrang;
        }
        else if ($current_page < 1){
            $current_page = 1;
        }
        // Tìm Start
            $start = ($current_page - 1) * $limit;
        // BƯỚC 5: TRUY VẤN LẤY DANH SÁCH TIN TỨC
        // Có limit và start rồi thì truy vấn CSDL lấy danh sách tin tức
            $sql = "SELECT * FROM $table LIMIT $limit offset $start";
            $result = mysqli_query($conn,$sql);
            require_once('Views/admin/QuanLySanPham/index.php');
            break;
        }
        case 'tim-kiem':{
        // KẾT NỐI 
             $tenmaychu='localhost';
             $tentaikhoan='root';
             $pass='';
             $csdl='wad_shop';
             $conn=mysqli_connect($tenmaychu, $tentaikhoan, $pass, $csdl);
        //Chức năng tìm kiếm
            if(isset($_GET['tukhoa'])){
                $key = $_GET['tukhoa'];
            }else{
                $key = "";
            }
            $table = "product";
            $timtheo = "name"; //tìm kiếm theo tên = name , theo giá = amount ,theo loai = quantity..
            $where = "WHERE name LIKE '%$key%' OR id = '$key'";
            $sql = "SELECT * FROM $table $where ORDER BY $timtheo DESC";
            $result_timkiem = mysqli_query($conn,$sql);
            //echo $sql."<br>";

            require_once('Views/admin/timkiem/index.php');
            break;
        

        }
        case 'quan-ly-order':{
            require_once('Views/admin/Order/index.php');
            break;
        }
        case 'quan-ly-users':{
            $table = "user";
            $data = $db->getAllData($table);
            require_once('Views/admin/Users/index.php');   
            break;
        }
        case 'sua-user':{
            $table = "user";
            $id =  "`id`=".$_GET['id'];
            $listLevel = $db->getAllData('level');
            $data = $db->getDataByOption($table,$id);
            editUser($db);
            require_once('Views/admin/SuaUser/index.php');   
            break;
        }
        case 'delete_danhmuc':{
            if(isset($_GET['id'])){
                $id=$_GET['id'];
                $tblTable = "category";
                if($db->deleteData($id,$tblTable)){

                    header('location:index.php?controller=admin&action=index');
                   
                }
                else{
                    header('location:index.php?controller=admin&action=index');
                }
            }
           
            break;
        }
        case 'delete_sp':{
            if(isset($_GET['id'])){
                $id=$_GET['id'];
                $tblTable = "product";
                if($db->deleteData($id,$tblTable)){

                    header('location:index.php?controller=admin&action=quan-ly-san-pham');
                   
                }
                else{
                    header('location:index.php?controller=admin&action=quan-ly-san-pham');
                }
            }
           
            break;
        }
        case 'delete_User':{
            if(isset($_GET['id'])){
                $id=$_GET['id'];
                $tblTable = "user";
                if($db->deleteData($id,$tblTable)){

                    header('location:index.php?controller=admin&action=Users');
                   
                }
                else{
                    header('location:index.php?controller=admin&action=Users');
                }
            }
        }
        default:{
            require_once('Views/admin/ThemDanhMuc/index.php');
            break;
        }
    }

?>