<?php
    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }else{
        $action = '';
    }
    $tableProduct = 'product';
    switch($action){
        case 'index':{
            require_once('Views/client/GioHang.php');
            break;
        }
        case 'add':{
            if(isset($_GET['id'])){
                $condition = "`id`=".$_GET['id'];
                $result = $db->getDataByOption($tableProduct,$condition);
                if(isset($_SESSION['cart']) ){
                    if(isset($_SESSION['cart'][$result->id])){
                        $_SESSION['cart'][$result->id]->soluong += 1;
                        $_SESSION['cart'][$result->id]->soluong = $_SESSION['cart'][$result->id]->soluong > 5 ? 5 : $_SESSION['cart'][$result->id]->soluong;
                        $_SESSION['cart'][$result->id]->thanhtoan = $_SESSION['cart'][$result->id]->soluong * $_SESSION['cart'][$result->id]->amount;
                    }else{
                        $result->soluong = 1;
                        $result->thanhtoan = $result->amount;
                        $_SESSION['cart'][$result->id] = $result;
                    }
                }else{
                    $_SESSION['cart'] = array();
                    $result->soluong = 1;
                    $result->thanhtoan = $result->amount;
                    $_SESSION['cart'][$result->id] = $result;
                }
            }
            header("location:?controller=giohang");
            break;
        }
        case 'remove':{
            if(isset($_SESSION['cart']) && isset($_GET['id'])){
                unset($_SESSION['cart'][$_GET['id']]);
            }
            header("location:?controller=giohang");
            break;
        }
        case 'fixsoluong':{
            if(isset($_GET['id']) && isset($_GET['soluong'])){
                $id = $_GET['id'];
                $soluong = (int) $_GET['soluong'];
                if($_SESSION['cart'][$id]){
                    if($soluong > 5) $soluong = 5;
                    if($soluong < 1) $soluong = 1;
                    $_SESSION['cart'][$id]->soluong = $soluong;
                    $_SESSION['cart'][$id]->thanhtoan = $soluong * $_SESSION['cart'][$id]->amount;
                    $total = 0;
                    foreach($_SESSION['cart'] as $value){
                        $total += $value->thanhtoan;
                    }
                    $_SESSION['cart'][$id]->total = $total;
                    echo json_encode($_SESSION['cart'][$id]);
                }else{
                    echo '0';
                }
            }
            break;
        }
        default:{
            require_once('Views/client/GioHang.php');
            break;
        }
    }

?>