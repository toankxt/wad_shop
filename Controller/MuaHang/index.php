<?php
    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }else{
        $action = '';
    }
    switch($action){
        case 'index':{
            $error = null;
            if(!isset($_SESSION['user'])){
                $error = 'Bạn vui lòng đăng nhập trước khi mua hàng';
                return require_once('Views/client/GioHang.php');
            }
            if(!isset($_SESSION['cart']) || count($_SESSION['cart']) == 0){
                $error = 'Chưa có sản phẩm nào để thanh toán';
                return require_once('Views/client/GioHang.php');
            }
            require_once('Views/client/DatHang.php');
            break;
        }
        case 'dathang':{
            print_r($_POST);
            print_r($_SESSION['cart']);
            require_once('Views/client/DatHang.php');
            break;
        }
        default:{
            require_once('Views/client/GioiThieu/index.php');
            break;
        }
    }

?>