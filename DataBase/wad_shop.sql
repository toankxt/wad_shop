-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 25, 2018 lúc 02:10 AM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `wad_shop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `createdAt`) VALUES
(2, 'MacBook', '2018-11-29 01:22:18'),
(38, 'Máy tính để bàn', '2018-12-25 08:50:32'),
(40, 'linh kiện máy tính', '2018-12-25 09:06:22'),
(41, 'ipad', '2018-12-25 14:22:08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `level`
--

INSERT INTO `level` (`id`, `name`, `createdAt`) VALUES
(1, 'Admin', '2018-11-27 15:44:04'),
(4, 'Thành Viên', '2018-11-27 15:44:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `fullname` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `product` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `pay` varchar(128) COLLATE utf8_unicode_ci DEFAULT 'Thanh toán khi nhận hàng',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `category`, `name`, `image`, `quantity`, `amount`, `description`, `createdAt`) VALUES
(91, 38, 'PC1', 'assets/uploads/pc2-1545728455.jpg', 10, 15000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:00:55'),
(92, 38, 'PC2', 'assets/uploads/pc1-1545728480.jpg', 3, 22000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:01:20'),
(93, 38, 'PC3', 'assets/uploads/pc3-1545729426.jpg', 12, 10000, 'Thông tin sản phẩm', '2018-12-25 09:01:43'),
(94, 38, 'PC4', 'assets/uploads/pc4-1545728522.jpg', 10, 30000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:02:02'),
(95, 38, 'PC5', 'assets/uploads/pc5-1545728544.jpg', 12, 35000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:02:24'),
(96, 38, 'PC6', 'assets/uploads/pc6-1545728563.jpg', 12, 22000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:02:43'),
(97, 38, 'PC7', 'assets/uploads/pc7-1545728579.jpg', 12, 22000, '<ul>\r\n	<li>- CPU: Core i5 Core i5 8400</li>\r\n	<li>- RAM/ HDD: 8Gb (1x8Gb)/ 1Tb</li>\r\n	<li>- VGA: VGA onboard, Intel HD Graphics</li>\r\n	<li>- OS: Ubuntu</li>\r\n</ul>\r\n', '2018-12-25 09:02:59'),
(99, 40, 'LK1', 'assets/uploads/LK1-1545729177.jpg', 10, 15000, 'Thông tin sản phẩm', '2018-12-25 09:11:12'),
(100, 40, 'LK2', 'assets/uploads/LK2-1545729299.jpg', 10, 10000, 'Thông tin sản phẩm', '2018-12-25 09:14:07'),
(101, 2, 'MacBook 1', 'assets/uploads/Mac1-1545746779.jpg', 10, 10000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:06:19'),
(102, 2, 'MacBook 2', 'assets/uploads/Mac2-1545746799.jpg', 12, 10000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:06:39'),
(103, 2, 'MacBook 3', 'assets/uploads/mac3-1545746821.jpg', 12, 22000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:07:01'),
(104, 2, 'MacBook 4', 'assets/uploads/mac4-1545746844.jpg', 11, 30000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:07:24'),
(105, 2, 'MacBook 5', 'assets/uploads/mac5-1545746865.jpg', 10, 10000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:07:45'),
(106, 2, 'MacBook 6', 'assets/uploads/mac6-1545746884.jpg', 12, 15000, '<h3>Thực ra, bạn c&oacute; n&ecirc;n b&aacute;n laptop cũ để mua laptop mới kh&ocirc;ng?</h3>\r\n\r\n<p>Như một điều tất yếu của luật kinh doanh, mọi thương hiệu c&ocirc;ng nghệ đều đổ tiền cho c&aacute;c chiến dịch quảng c&aacute;o để thuyết phục kh&aacute;ch h&agrave;ng rằng những điều mới mẻ trong c&aacute;c d&ograve;ng laptop mới l&agrave; cần thiết. Tuy nhi&ecirc;n trong b&agrave;i viết n&agrave;y, Phong Vũ muốn khuy&ecirc;n bạn rằng việc b&aacute;n laptop cũ để mua laptop mới l&agrave; kh&ocirc;ng th&iacute;ch đ&aacute;ng.</p>\r\n\r\n<ul>\r\n	<li><strong>Tại sao kh&ocirc;ng n&ecirc;n b&aacute;n laptop cũ?</strong></li>\r\n</ul>\r\n\r\n<p>Với tốc độ ph&aacute;t triển đ&aacute;ng sợ của c&ocirc;ng nghệ hiện nay, việc c&aacute;c d&ograve;ng laptop mới li&ecirc;n tục ra mắt sẽ khiến cho laptop cũ bị tụt gi&aacute; th&ecirc; thảm chỉ sau một v&agrave;i năm c&oacute; mặt tr&ecirc;n thị trường. Thường th&igrave; một chiếc laptop được thiết kế cho mục đ&iacute;ch sử dụng &iacute;t nhất l&agrave; 3 năm. Nhưng chỉ tới năm thứ hai l&agrave; c&aacute;c mẫu laptop mới sẽ ra mắt nhằm lung lạc người ti&ecirc;u d&ugrave;ng rằng sản phẩm m&agrave; họ mua năm ngo&aacute;i đ&atilde; lỗi thời.</p>\r\n', '2018-12-25 14:08:04'),
(107, 41, 'ipad1', 'assets/uploads/ip1-1545747800.jpg', 12, 30000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:23:20'),
(108, 41, 'ipad2', 'assets/uploads/ip5-1545747837.jpg', 11, 10000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:23:57'),
(109, 41, 'ipad4', 'assets/uploads/ip4-1545747869.jpg', 11, 10000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:24:29'),
(110, 40, 'LK3', 'assets/uploads/LK3-1545747983.jpg', 12, 15000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:26:23'),
(111, 38, 'PC8', 'assets/uploads/pc10-1545748011.jpg', 12, 10000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:26:51'),
(112, 41, 'ipad5', 'assets/uploads/ip2-1545748041.jpg', 11, 22000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:27:21'),
(113, 38, 'PC9', 'assets/uploads/pc12-1545748074.jpg', 12, 10000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:27:54'),
(114, 40, 'LK4', 'assets/uploads/LK4-1545748216.jpg', 12, 4000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:30:16'),
(115, 38, 'PC10', 'assets/uploads/pc11-1545748247.jpg', 11, 22000, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2018-12-25 14:30:47'),
(116, 40, 'LK9', 'assets/uploads/LK5-1545748281.jpg', 12, 4500, '<p>Probook 450 G5 c&ograve;n sở hữu tương đối đầy đủ c&aacute;c cổng kết nối th&ocirc;ng dụng hiện nay để người d&ugrave;ng c&oacute; thể dễ d&agrave;ng kết nối với c&aacute;c thiết bị ngoại vi b&ecirc;n ngo&agrave;i. Cụ thể, cạnh tr&aacute;i của m&aacute;y c&oacute; sự xuất hiện của khe tản nhiệt, 1 cổng USB 3.0, jack cắm tai nghe 3.5 mm v&agrave; khe đầu đọc thẻ nhớ SD. Trong khi canh phải của m&aacute;y l&agrave; nơi đặt cổng nguồn, cổng mạng LAN, cổng VGA, cổng HDMI, 1 cổng USB 3.0 v&agrave; 1 cổng USB-C.</p>\r\n\r\n<p>Sở hữu m&agrave;n h&igrave;nh 14 inches với độ ph&acirc;n giải FHD 1920 x 1080 pixels v&agrave; khả năng chống ch&oacute;i tốt, HP Probook G5 tự tin đem đến cho bạn những h&igrave;nh ảnh sắc n&eacute;t v&agrave; sống động. Chất lượng m&agrave;u sắc tổng thể tuy chỉ dừng lại ở mức trung b&igrave;nh do hạn chế về độ tương phản v&agrave; g&oacute;c nh&igrave;n, nhưng qu&aacute; tr&igrave;nh kiểm tra cho thấy HP ProBook 440 G5 kh&ocirc;ng hề bị hở s&aacute;ng m&agrave;n h&igrave;nh.</p>\r\n\r\n<p>ProBook 440 G5 được HP trang bị bộ xử l&yacute; Intel Core i5-8250U c&ugrave;ng &quot;trợ thủ&quot; RAM DDR4 2400MHz với dung lượng 4GB. Chip i5-8250U thuộc d&ograve;ng Coffee Lake mới của Intel được tăng số lượng nh&acirc;n xử l&yacute; l&ecirc;n gấp đ&ocirc;i, song mức xung nhịp chuẩn lại c&oacute; phần thấp hơn chỉ 1.6GHz. Về đồ họa, mẫu CPU thế hệ mới tuy mang tr&ecirc;n m&igrave;nh GPU Intel UHD Graphics 620 nhưng thực chất vẫn kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với nh&acirc;n đồ họa Intel HD Graphics 620.</p>\r\n', '2018-12-25 14:31:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` bit(1) NOT NULL,
  `level` int(11) NOT NULL,
  `username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `email`, `fullname`, `createdAt`, `address`, `birthday`, `gender`, `level`, `username`, `password`) VALUES
(3, 'admin@gmail.com', 'Trần Văn Cương', '2018-11-27 14:56:51', 'Hà Nội', '1998-06-19', b'1', 1, 'vanxcuong', 'e10adc3949ba59abbe56e057f20f883e'),
(4, 'vancuong@gmail.com', 'Trần Văn Cương', '2018-12-25 01:08:09', NULL, '1990-11-10', b'1', 4, '', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_OrderProduct` (`product`),
  ADD KEY `FK_OrderUser` (`user`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ProductCategory` (`category`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `FK_LevelUser` (`level`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT cho bảng `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_OrderProduct` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_OrderUser` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_ProductCategory` FOREIGN KEY (`category`) REFERENCES `category` (`id`);

--
-- Các ràng buộc cho bảng `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_LevelUser` FOREIGN KEY (`level`) REFERENCES `level` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
