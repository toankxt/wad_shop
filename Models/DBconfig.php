<?php
    class Database{
        private $hostname = 'localhost';
        private $username = 'root';
        private $pass = '';
        private $dbname = 'wad_shop';
        private $conn = NULL;
        private $result = NULL;
        // Connect database
        public function connect(){
            $this->conn = new mysqli($this->hostname, $this->username, $this->pass, $this->dbname);
            if(!$this->conn){
                echo 'Kết nối CSDL thất bại';
                exit();
            }else{
                mysqli_set_charset($this->conn, 'utf8');
            }
            return $this->conn;
        }
        // handle query data
        public function execute($sql){
            $this->result = $this->conn->query($sql);
            return $this->result;
        }
        // handle get data
        public function getData(){
            if($this->result){
                $data = mysqli_fetch_array($this->result);
            }else{
                $data = 0;
            }
            return $data;
        }
        // Lấy tổng số lượng
        public function count_document($table,$condition=NULL){
            $condition = isset($condition) ? " where $condition" : "";
            $sql = "select count('id') as 'count' from $table $condition";
            $this->execute($sql);
            return mysqli_fetch_object($this->result)->count;
        }
        // đếm số lượng kết quả trả vêf
        public function num_rows(){
            if($this->result){
                $num = mysqli_num_rows($this->result);
            }else{
                $num = 0;
            }
            return $num;
        }
        // get all data
        public function getAllData($table){
            $sql = "select * from $table";
            $this->execute($sql);
            if($this->num_rows() == 0){
                $data = 0;
            }else{
                while($datas = $this->getData()){
                    $data[] = $datas;
                }
            }
            return $data;
        }
        // query theo yêu cầu
        public function queryData($table,$limit = NULL, $offset = NULL, $sort = NULL,$condition = NULL){
            $condition = isset($condition) ? " where $condition" : "";
            $limit = isset($limit) ? " limit $limit" : "";
            $offset = isset($offset) ?" offset $offset" : "";
            $sort = isset($sort) ?" order by $sort" : "";
            $sql = "select * from $table $condition $sort $limit $offset ";
            $this->execute($sql);
            if($this->num_rows() == 0){
                $data = 0;
            }else{
                while($datas = $this->getData()){
                    $data[] = $datas;
                }
            }
            return $data;
        }
        // get ID
        public function getDataByOption($table,$condition){
            $sql = "select * from $table where $condition";
            $this->execute($sql);
            return mysqli_fetch_object($this->result);
        }
        // add document
        public function insertData($table,$column,$value){
            $sql = "insert into $table($column) values($value)";
            return $this->execute($sql);
        }
        // update document
        public function updateData($table,$condition,$value){
            $sql = "update $table set $value where $condition";
            return $this->execute($sql);
        }
        //delete document
        public function deleteData($id,$table){
            $sql = "delete from $table where id = '$id'";
            return $this->execute($sql);
        }

        
        
    }
?>