# Bài tập lớn: Phát triển ứng dụng Website
# Tên đề tài: Nghiên cứu và phát triển ứng dụng Website bán hàng Online ( WAD_SHOP )

# Cài đặt dự án
- Bước 1: Mở cmd -> cd tới thư mục C:\xampp\htdocs ( Mặc định xampp thường ở ổ C. Bạn để xampp ở ổ khác thì cd tới đó và đi vào thư mục htdocs)
- Bước 2: Nhập trong cmd :
    + clone bằng HTTPS: git clone https://gitlab.com/toankxt/wad_shop.git
    + clone bằng SSH : git clone git@gitlab.com:toankxt/wad_shop.git
- Bước 3: Mở xampp:
  + Start Apache
  + Start MySQL
- Bước 4: Chạy ứng dụng
  + http://localhost:port/wad_shop/

# Cài đặt CSDL
- Bước 1: Truy cập http://localhost:8080/phpmyadmin
- Bước 2: Tạo database tên: wad_shop
- Bước 3: Click vào database wad_shop nhìn sang bên phải có nhấn vào "Nhập(import)"
- Bước 4: Chọn file "wad_shop.sql" trong folder Database
- Bước 5: Nhấn thực hiện bên dưới và xem kết quả.
