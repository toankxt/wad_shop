<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 list-info">
                        <?php include('Views/admin/Menu/index.php'); ?>
                    </div>
<div class="col-sm-9 col-xs-12 manager-order mt-3">
        <div class="row">
            <div class="jumbotron bg-white btn-block">
                <h1 class="display-3">Các đơn đặt hàng</h1>
                
            </div>
            <table class="table table-striped table-bordered table-responsive-sm mt-3">
                <thead class="thead-light ">
                    <tr>
                        <th>Khách Hàng</th>
                        <th>Ngày đặt</th>
                        <th>Hình ảnh</th>
                        <th>Tên sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Tổng cộng</th>
                        <th>Trạng thái</th>
                        <th>Khác</th>
                    </tr>
                </thead>
                <tbody id="reloads">
                <tr>
                    <td><a href="">abc</a></td>
                    <td>09:16 - 22/03/2018</td>
                    <td>
                        <img src="assets/images/4.jpg" width="50" alt="">
                    </td>
                    <td>Đồng Hồ Orient Loại 3</td>
                    <td>6</td>
                    <td>38.304.000 VNĐ</td>
                    <td>
                        <a href="" class="fix-status" >Chờ giao hàng</a>
                        <select name="successStatus" class="form-control" style="display:none">
                            <option value="0">Chờ giao hàng</option>
                            <option value="1">Giao hàng thành công</option>
                            <option value="2">Hủy đơn hàng</option>
                        </select>
                    </td>
                    <td><a href="#">Chi tiết</a></td>
                </tr>
                
                    
                </tbody>
            </table>
        </div>
    </div>
    <!-- managerinfo col:12 -->

                </div>
                <!-- row -->
            </div>
            <!-- col:12 -->
        </div>
    </div>
    <!-- container -->
</main>
<?php include('Views/client/footer.php'); ?>