<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-sm-3 col-xs-12 list-info">
						<?php include('Views/admin/Menu/index.php'); ?>
					</div>
    <div class="col-sm-9 col-xs-12 manager-product mt-3">
        <div class="row">
            <div class="jumbotron bg-white btn-block">
                <h1 class="display-3">Quản Lý Sản Phẩm</h1>
                	<form action="" method="GET">
						<table>
							<input type="hidden" name="controller" value="admin">
							<input type = text name = 'tukhoa'  placeholder="nhập từ khóa ">
							<input type = submit value = 'Tìm'>
						</table>
							<input type="hidden" name="action" value="tim-kiem">
					</form>

            </div>
		
			<div class="col-sm-12 show-sp mt-3">
			<?php 
			// foreach($data as $i){
				while ($row = mysqli_fetch_assoc($result)){
			 ?>
					<div class="col-sm-4 mt-3 col-xs-12 sanpham">
							<img src="<?php echo $row['image']; ?>">
							<div class="ten"><?php echo $row['name']; ?></div>
							<div class="gia"><?php echo $row['amount']; ?>$</div>
						<div class="card-footer">
							<a  href="index.php?controller=admin&action=sua-san-pham&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Chỉnh Sửa</a>
							<a onclick="return confirm('Bạn có muốn xóa không ?') " href="index.php?controller=admin&action=delete_sp&id=<?php echo $row['id']; ?>" ><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
						</div>
					</div> <!--end sản phẩm -->
			<?php
			// }
		}
			?>
			</div>	
            </div>
				
        
		<div class="phantrang">
			<?php
			// BƯỚC 7: HIỂN THỊ PHÂN TRANG
			// nếu current_page > 1 và total_page > 1 mới hiển thị nút prev
			if ($current_page > 1 && $tongsotrang > 1){
			    echo '<a href="index.php?controller=admin&action=quan-ly-san-pham&page='.($current_page-1).'">Prev</a> | ';
			}
			// Lặp khoảng giữa
			for ($i = 1; $i <= $tongsotrang; $i++){
			// Nếu là trang hiện tại thì hiển thị thẻ span
			// ngược lại hiển thị thẻ a
			    if ($i == $current_page){
			        echo '<span>'.$i.'</span> | ';
			    }
			    else{
			        echo '<a href="index.php?controller=admin&action=quan-ly-san-pham&page='.$i.'">'.$i.'</a> | ';
			    }
			}
			// nếu current_page < $total_page và total_page > 1 mới hiển thị nút prev
			if ($current_page < $tongsotrang && $tongsotrang > 1){
			    echo '<a href="index.php?controller=admin&action=quan-ly-san-pham&page='.($current_page+1).'">Next</a> | ';
			}
			?>
		</div>
    </div>
</div>
		
</main>
<?php include('Views/client/footer.php'); ?>
</body>
</html>