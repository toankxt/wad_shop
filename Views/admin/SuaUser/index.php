<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="assets/vendor/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/1.js"></script>
    <link rel="stylesheet" href="assets/vendor/bootstrap.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome.css">
    <link rel="stylesheet" href="assets/css/1.css">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One&amp;subset=latin-ext" rel="stylesheet">
  <title>Admin</title>
</head>
<body>
<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 list-info">
                        <?php include('Views/admin/Menu/index.php'); ?>
                    </div>
                    <div class="col-sm-9 col-xs-12 admin-info mt-3">
                        <div class="row">
                            <div class="jumbotron bg-white btn-block">
                                <h1 class="display-3">Sửa Thông Tin Người Dùng</h1>
                                <p class="lead"></p>
                                <hr class="my-2">
                                <form action="#" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="">Tên</label>
                                        <input type="text" name="fullname"  class="form-control" placeholder="" value="<?php echo "$data->fullname"; ?>">
                                        <small id="helpId" class="text-muted">Help text</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Địa Chỉ</label>
                                        <input type="text" name="address"  class="form-control" placeholder="" value="<?php echo "$data->address"; ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label style="margin-right: 15px;">Giới Tính: <?php echo $data->gender?></label>
                                        <input type="radio" value="1" name="gender" <?php echo $data->gender == 1 ? "checked":"" ?>> <label for=""> Nam</label>
                                        <input type="radio" value="0" name="gender" <?php echo $data->gender == 0 ? "checked":"" ?>> <label for=""> Nữ</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Level</label>
                                        <select name="level" class="form-control">
                                            <?php  foreach($listLevel as $level){
                                                if($level['id']==$data->level)
                                                    $option = "<option selected value=".$level['id'].">".$level['name'];
                                                else
                                                    $option = "<option value=".$level['id'].">".$level['name'];
                                                echo $option;
                                            };?>
                                        </select>
                                    </div>
                                    <?php if(isset($error)){
                                        echo "<div class='form-group'>
                                                <div style='text-align: center;color: red;'></div>
                                            </div>";
                                        }
                                    ?>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-danger btn-block" name="suauser">Cập Nhật</button>
                                    </div>

                                </form>
                            </div>
                            <!-- jumbotron -->
                            
                        </div>
                    </div>
                    <!-- managerinfo col:12 -->
                    <script>
                        $(document).ready(function () {
                            editor("editor");
                            editor("editor1");
                            editor("editor2");
                        });
                        function editor(id){
                            
                            CKEDITOR.replace( ''+id+'', {
                                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images'
                            });
                        }
                    </script>
                </div>
                <!-- row -->
            </div>
            <!-- col:12 -->
        </div>
    </div>
    <!-- container -->
</main>
<?php include('Views/client/footer.php'); ?>
</body>
</html>