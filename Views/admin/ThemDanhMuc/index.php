<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 list-info">
                        <?php include('Views/admin/Menu/index.php'); ?>
                    </div>
                 
                    <div class="col-sm-9 col-xs-12 admin-info mt-3">
                        <div class="row">
                            <div class="jumbotron bg-white btn-block">
                                <h1 class="display-3">Thêm Danh Mục</h1>
                                <hr class="my-2">
                                <form action="" method="POST">
                                    <div class="form-group">
                                    <label for="">Nhập Danh Mục</label>
                                    <input type="text" name="name" class="form-control" placeholder="Tên danh mục" aria-describedby="helpId">
                                    </div>
                                    <div class="form-group">
                                    <input type="submit" class="form-control btn btn-outline-danger" name="them" value="Thêm" >
                                    <?php
                                     if(isset($themthanhcong) && in_array('add_success',$themthanhcong)){
                                         echo "<p style='color:green; text-align:center; '>Thêm mới thành công !</p>";
                                     }
                                    ?>
                                    </div>
                                </form>
                            </div>
                            
           
           <!-- Show dabase ra  -->

            <div class="jumbotron bg-white btn-block mt-3">
                <h1 class="display-3">Quản lý danh mục</h1>
                <p class="lead"></p>
                <hr class="my-2">
                <ul class="list-group mt-3">
                    <?php
                    $stt = 1;
                    foreach($data as $value){
                    ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center ">
                        <?php 
                            echo $stt," . ";
                            echo $value['name']; 
                        ?>
                        <span class="badge badge-secondary badge-pill" >
                            <a onclick="return confirm('Bạn có muốn xóa không ?') " href="index.php?controller=admin&action=delete_danhmuc&id=<?php echo $value['id']; ?>">Xóa</a>
                        </span>
                    </li>
                    <?php
                        $stt++;
                    }
                    ?>
                </ul>
            </div>
            <!-- jumbotron -->
        </div>
    </div>
    <!-- managerinfo col:12 -->
                </div>
                <!-- row -->
            </div>
            <!-- col:12 -->
        </div>
    </div>
    <!-- container -->
</main>
<?php include('Views/client/footer.php'); ?>
</body>
</html>