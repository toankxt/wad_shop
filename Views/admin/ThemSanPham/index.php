
<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 list-info">
                        <?php include('Views/admin/Menu/index.php'); ?>
                    </div>
                    <div class="col-sm-9 col-xs-12 admin-info mt-3">
                        <div class="row">
                            <div class="jumbotron bg-white btn-block">
                                <h1 class="display-3">Thêm Sản Phẩm</h1>
                                <p class="lead"></p>
                                <hr class="my-2">
                                <form action="#" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label  >Danh Mục</label>
                                        <select class="form-control" name="category">
                                            <?php
                                                foreach($data_loai as $i){
                                            ?>
                                                <option value="<?php echo $i['id']?>"><?php echo $i['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label for="">Tên Sản Phẩm</label>
                                    <input type="text" name="name"  class="form-control" placeholder="" required >
                                    <small id="helpId" class="text-muted">Help text</small>
                                    </div>
                                    <div class="form-group">
                                    <label for="">Hình ảnh</label>
                                    <input type="file" name="image" class="form-control">
                                    </div> 
                                    <div class="form-group">
                                    <label for="">Số lượng</label>
                                    <input type="text" name="quantity" pattern="[0-9]{1,}" class="form-control" placeholder="Số lượng sản phẩm" aria-describedby="helpId">
                                    <small id="helpId" class="text-muted"></small>
                                    </div>
                                    <div class="form-group">
                                    <label for="">Giá sản phẩm</label>
                                    <input type="text" name="amount" required pattern="[0-9]{4,}" class="form-control" placeholder="Giá gốc sản phẩm" aria-describedby="helpId">
                                    <small id="helpId" class="text-muted">VD:999999</small>
                                    </div>
                                    <div class="form-group">
                                    <label for="editor1">Thông tin sản phẩm</label>
                                        <textarea name="description" id="editor">Thông tin sản phẩm</textarea>
                                    <small id="helpId" class="text-muted">Help text</small>
                                    </div>
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-outline-danger btn-block" name="them">Thêm Sản Phẩm</button>
                                    
                                    </div>
                                </form>
                            </div>
                            <!-- jumbotron -->
                        </div>
                    </div>
                    <!-- managerinfo col:12 -->
                    <script src="assets/js/ckeditor/ckeditor.js"></script>
                    <script>
                        $(document).ready(function () {
                            editor("editor");
                        });
                        function editor(id){
                            CKEDITOR.replace( ''+id+'', {
                                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?Type=Images'
                            });
                        }
                    </script>
                </div>
                <!-- row -->
            </div>
            <!-- col:12 -->
        </div>
    </div>
    <!-- container -->
</main>
<script src="assets/js/jquery.js"></script>
<?php include('Views/client/footer.php'); ?>
</body>
</html>