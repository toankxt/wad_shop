<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="assets/vendor/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/1.js"></script>
    <link rel="stylesheet" href="assets/vendor/bootstrap.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome.css">
    <link rel="stylesheet" href="assets/css/1.css">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One&amp;subset=latin-ext" rel="stylesheet">
  <title>Admin</title>
</head>
<body>
<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-12 list-info">
                        <?php include('Views/admin/Menu/index.php'); ?>
                    </div>


               <div class="col-sm-9 col-xs-12 manager-order manager-users mt-3">
        <div class="row">
            <div class="jumbotron bg-white btn-block">
                <h1 class="display-3">Danh sách khách hàng</h1>
            </div>
            <table class="table table-striped table-bordered table-responsive-sm ">
                <thead class="thead-light ">
                    <tr>
                        <th>Tên đầy đủ </th>
                        <th>email </th>
                        <th>Địa chỉ</th>
                        <th>Tên đăng nhập</th>
                        <th>Cấp bậc</th>
                        <th>Tùy chọn</th>
                    </tr>
                </thead>
                <tbody id="#">
                    
                <?php
                foreach($data as $i){
                ?>
                    <tr>
                        <td class="text-center"><a href=""><?php echo $i['fullname'] ?></a></td>
                        <td><?php echo $i['email'] ?></td>
                        <td><?php echo $i['address'] ?></td>
                        <td><?php echo $i['username'] ?></td>
                        <td><?php echo $i['level'] ?></td>
                        <td>
                            <div class="behavior">
                                <a href="index.php?controller=admin&action=sua-user&id=<?php echo $i['id']; ?>" class="float-left btn btn-outline-success edit-user">Chỉnh sửa</a>
                                <a href="index.php?controller=admin&action=delete_User&id=<?php echo $i['id']; ?>" class="float-right btn btn-outline-danger del-user">Xóa</a>
                            </div>
                            <div class="behavior-success" style="display:none">
                                <a href=""class="float-right btn btn-outline-success btn-block save-user"  idu="5aa73cdfdf0d801a989b822e">Lưu</a>
                            </div>
                        </td>
                    </tr>

                    <?php
                    }
                    ?>
            
                </tbody>
            </table>
            <div class="Hu-load text-center col-12" style="margin:auto;display:none;">
                    <img src="/images/o2mtzbxKtid.gif"  alt="">
            </div>
            <div class="checksp text-center" style="margin:auto; width:100%"></div>
            
        </div>
    </div>
    <!-- managerinfo col:12 -->

                </div>
                <!-- row -->
            </div>
            <!-- col:12 -->
        </div>
    </div>
    <!-- container -->
</main>

<?php include('Views/client/footer.php'); ?>