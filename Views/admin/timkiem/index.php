<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="assets/vendor/bootstrap.js"></script>
 	<script type="text/javascript" src="assets/js/1.js"></script>
	<link rel="stylesheet" href="assets/vendor/bootstrap.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome.css">
 	<link rel="stylesheet" href="assets/css/1.css">
 	<link href="https://fonts.googleapis.com/css?family=Black+Ops+One&amp;subset=latin-ext" rel="stylesheet">
  <title>Admin</title>
</head>
<body>
<?php include('Views/client/header.php'); ?>
<main class="main" id="users">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-sm-3 col-xs-12 list-info">
						<?php include('Views/admin/Menu/index.php'); ?>
					</div>
    <div class="col-sm-9 col-xs-12 manager-product mt-3">
        <div class="row">
            <div class="jumbotron bg-white btn-block">
                <h1 class="display-3">Quản Lý Sản Phẩm</h1>
                	<form action="" method="GET">
						<table>
							<input type="hidden" name="controller" value="admin">
							<input type = text name = 'tukhoa' placeholder="nhập từ khóa ">
							<input type = submit value = 'Tìm'>
						</table>
							<input type="hidden" name="action" value="tim-kiem">
					</form>
            </div>
		
			<div class="col-sm-12 show-sp mt-3">
				<?php 
			// foreach($data as $i){
				while ($row = mysqli_fetch_assoc($result_timkiem)){
			 ?>
					<div class="col-sm-4 mt-3 col-xs-12 sanpham">
							<img src="<?php echo $row['image']; ?>">
							<div class="ten"><?php echo $row['name']; ?></div>
							<div class="gia"><?php echo $row['amount']; ?>$</div>
						<div class="card-footer">
							<a  href="index.php?controller=admin&action=sua-san-pham&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Chỉnh Sửa</a>
							<a onclick="return confirm('Bạn có muốn xóa không ?') " href="index.php?controller=admin&action=delete_sp&id=<?php echo $row['id']; ?>" ><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
						</div>
					</div> <!--end sản phẩm -->
			<?php
			// }
			}
			?>
			</div>
    </div>
</div>
		
</main>
<?php include('Views/client/footer.php'); ?>
</body>
</html>