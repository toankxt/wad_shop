<div class="col-sm-9">
	<div class="row">
		<div class="col-sm-6">
			<a href="#" class="img">
				<img src="<?php echo $data->image ?>" height="400" alt="không có ảnh">
			</a>
		</div>
		<div class="col-sm-6">
			<h4 style="text-transform: capitalize;"><?php echo $data->name ?></h4> <hr>
			<p>Giá : <?php echo number_format($data->amount); ?> VNĐ</p>
			<p>Bảo hành: 12 tháng</p>
			<p>Tình trạng:
				<?php if($data->quantity != 0){
					echo "<span>Còn hàng</span>";
				}else{
					echo "<span style='color:red'>Hết hàng</span>";
				}?>
			</p>
			<a href="?controller=giohang&action=add&id=<?php echo $data->id ?>">
				<div type="button" class="btn">Thêm vào giỏ hàng</div>
			</a>
		</div>
	</div>
	<div class="row thongso">
		<div  style="width:100%;" >
			<h6 style="width:100%;padding-left: 10px;" class="tt text-xs-left">Chi tiết sản phẩm :</h6>
		</div>
		<div  style="width:100%;padding-left:20px;">
			<?php echo $data->description; ?>
		</div>
		<div  style="width:100%;"  >
			<h6 style="width:100%;padding-left: 10px;" class="tt text-xs-left">Sản phẩm cùng loại :</h6>
		</div>
	</div> <!-- end thông tin kỹ thuật -->
	<div class="row box_pro">
		<?php if($SanPhamTuongTu != 0){ ?>
		<?php foreach ($SanPhamTuongTu as $key => $value) { ?>
		<div class="sanpham">
			<a href="?controller=chitiet&action=index&product=<?php echo to_slug($value['name']).'.'.$value['id'] ?>">
				<img src="<?php echo $value['image']; ?>">
				<div class="ten"><?php echo $value['name']; ?></div>
				<div class="gia">
					Giá: <?php  echo number_format($value['amount']); ?> VNĐ
				</div>
				<div class="button">xem chi tiết</div>
			</a>
		</div>
		<?php }}else echo '<h3>Sản phẩm chưa cập nhật</h3>'; ?>
	</div>
</div>