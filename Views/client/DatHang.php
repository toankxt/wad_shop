<?php include('header.php') ?>
<div id="main" style="padding-top:50px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="?controller=muahang&action=dathang" method="post" style="width:100%;">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12 ">
                            <div class="card">
                                <h3 class="card-header">Thông tin khách hàng</h3>
                                <div class="card-block">
                                    <div class="container">
                                        <div class="row">
                                            <div class="form-group row" style="width:100%">
                                                <label for="name" class="col-sm-4 col-form-label text-right text-right">Người nhận hàng</label>
                                                <div class="col-sm-8">
                                                    <input class="form-control" name="name" id="customer" type="text" required   placeholder="Họ & tên">
                                                </div>
                                            </div>
                                            <div class="form-group  row" style="width:100%">
                                                <label for="address-info" class="col-sm-4 col-form-label text-right">Địa chỉ nhận hàng</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control"  placeholder="Địa chỉ nhận hàng" required id="addressorder" name="addressorder" id="address-info" style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group  row" style="width:100%">
                                                <label for="phoneNumber" class="col-sm-4 col-form-label text-right">Điện thoại</label>
                                                <div class="col-sm-8">
                                                    <input type="number" value="" name="phone" id="phoneNumber" required class="form-control" maxlength="10" placeholder="Số điện thoại"  aria-describedby="helpId">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- card-block -->
                            </div>
                            <!-- card -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="card">
                                <h3 class="card-header">Phương thức thanh toán</h3>
                                <div class="card-block">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="pay"  value="Thanh toán khi nhận hàng" checked >
                                                        Thanh toán khi nhận hàng
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" name="pay"  value="Chuyển khoản ngân hàng1" >
                                                        Chuyển khoản ngân hàng
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <small id="checkmethodPay" class="text-muted reNull"></small>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- card Phương thức thanh toán-->
                            <div class="card mt-4">
                                <h3 class="card-header">Check Sản Phẩm</h3>
                                <div class="card-block">
                                    <table class="table table-striped table-bordered table-responsive-sm" style="margin:0;">
                                        <thead class="thead-light ">
                                            <tr>
                                                <th>Hình ảnh</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Số lượng</th>
                                                <th>Giá/sản phẩm (VNĐ)</th>
                                                <th>Tổng tiền (VNĐ)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $total = 0;?>
                                            <?php if(isset($_SESSION['cart'])) foreach ($_SESSION['cart'] as $value){
                                                $total += $value->thanhtoan;
                                                echo "<tr>
                                                    <td><img src='$value->image' width='50' alt='Hình ảnh'></td>
                                                    <td>$value->name</td>
                                                    <td>$value->soluong</td>
                                                    <td>".number_format($value->amount)."</td>
                                                    <td>".number_format($value->thanhtoan)."</td>
                                                </tr>";
                                            }?>
                                            <tr style="background: #15ab12;border: 2px solid #868181;color: white;">
                                                <td class="text-xs-center" colspan=5>Thành Tiền: <?php echo number_format($total) ?> VNĐ </td> 
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- card-block -->
                            </div>
                            <!-- card -->
                        </div>
                        <!-- row -->
                        <div class="frames-success text-xs-center mt-3" style="width:100%;">
                            <input type="submit" class="btn btn-outline-danger" value="Đặt hàng"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
