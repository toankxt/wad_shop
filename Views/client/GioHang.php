<?php include('header.php') ?>

<main class="main mt-3" id="gio-hang">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="back-router"><a href="?controller=trangchu">< Tiếp tục mua hàng</a></div>
                <div class="row">
                    <div class="col-sm-9 col-xs-12 show-cart">
                        <h3 class="title-cart">Giỏ hàng của tôi</h3>
                        <p class="number-cart"><?php echo isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0; ?> sản phẩm</p>
                        <div class="frames-cart">
                            <table class="table table-striped table-bordered table-responsive-sm">
                                <thead class="thead-light ">
                                    <tr>
                                        <th>Hình ảnh</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>Giá/sản phẩm (VNĐ)</th>
                                        <th>Tổng tiền (VNĐ)</th>
                                        <th>Hành Động </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php  $total = 0;?>
                                        <?php if(isset($_SESSION['cart'])) foreach ($_SESSION['cart'] as $value){
                                            $total += $value->thanhtoan;
                                            $onclick = "onclick='if(!confirm(`Bạn có muốn xóa sản phẩm không`)) return false'";
                                            $option = '';
                                            for ($i=1; $i <= 5; $i++) {
                                                $option .= $i == $value->soluong ? "<option value='$i' selected>$i</option>" :"<option value='$i'>$i</option>";
                                            };
                                            echo "<tr>
                                                <td><img src='$value->image' width='50' alt='Hình ảnh'></td>
                                                <td>$value->name</td>
                                                <td>
                                                    <select name='number-cart' onchange='ChangeQuantity(this,$value->id)' class='form-control ChangeQuantity'>
                                                        ".$option."
                                                    </select>
                                                </td>
                                                <td>".number_format($value->amount)."</td>
                                                <td>".number_format($value->thanhtoan)."</td>
                                                <td class='text-center'>
                                                    <a ".$onclick." href='?controller=giohang&action=remove&id=$value->id'  class='btn btn-outline-danger' alt='Xóa sản phẩm'>Xóa</a>
                                                </td>
                                            </tr>";
                                        }?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- col-sm-9 -->
                    <div class="col-sm-3 col-xs-12 sum-cart">
                        <h4 class="info-cart">Thông tin đơn hàng</h4>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center active">
                                Tổng Tiền:
                                <span class="badge badge-secondary badge-pill total"><?php echo number_format($total); ?> </span> <span>VNĐ</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center ">
                               <a href="?controller=muahang&action=index" class="btn btn-outline-danger btn-block">Mua ngay</a>
                                <?php if(isset($error))
                                    echo "<p style='color:red;font-size:14px;' class='text-xs-center'>$error</p>";
                                ?>
                            </li>
                        </ul>
                    </div>
                    <!-- col-sm-3 -->
                </div>
                <!-- row -->
            </div>
            <!-- col-sm-12 -->
        </div>
    </div>
    <!-- container -->
</main>
<script>
function  ChangeQuantity(element,id) {
    let url = `?controller=giohang&action=fixsoluong&id=${id}&soluong=${element.value}`
    let elmTotal = document.querySelector(".total")
    loadMethodGet(url,res =>{
        let data = JSON.parse(res)
        elmTotal.innerHTML = data.total.toLocaleString('en')
        element.parentElement.nextElementSibling.nextElementSibling.innerHTML = data.thanhtoan.toLocaleString('en')
    })
}
</script>
<?php include('footer.php') ?>

