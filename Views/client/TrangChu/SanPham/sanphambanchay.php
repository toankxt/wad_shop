
<div class="boxLimit">
	<h5>
		<a href="#">sản phẩm máy tính bàn</a>
	</h5>
	<?php if($SanPhamMoiNhat['items'] != 0){ ?>
	<?php foreach ($SanPhamMoiNhat['items'] as $key => $value) { ?>
	<div class="sanpham">
		<a href="?controller=chitiet&action=index&product=<?php echo to_slug($value['name']).'.'.$value['id'] ?>">
			<img src="<?php echo $value['image']; ?>">
			<div class="ten"><?php echo $value['name']; ?></div>
			<div class="gia">
				Giá: <?php  echo number_format($value['amount']); ?> VNĐ
			</div>
			<div class="button">xem chi tiết</div>
		</a>
	</div>
	<?php }}else echo '<h3>Sản phẩm chưa cập nhật</h3>'; ?>
</div> <!-- end boxLimit -->
<div style="width: 100%;display: flex;justify-content: center;">
	<nav aria-label="Page navigation example">
	<ul class="pagination">
		<li class="page-item <?php echo ($SanPhamMoiNhat['page'] == 1 || $SanPhamMoiNhat['page'] == 0) ? 'hidden-xs-up':'' ?>">
			<a class="page-link" href="?controller=trangchu&active=index&page=<?php echo $SanPhamMoiNhat['page']-1; ?>" aria-label="Previous">
				<span aria-hidden="true">&laquo;</span>
				<span class="sr-only">Previous</span>
			</a>
		</li>
		<?php for ($i=1; $i <= $SanPhamMoiNhat['pages'] ; $i++) { ?>
			<li class="page-item <?php echo ($SanPhamMoiNhat['page'] == $i) ? 'active':'';?>">
				<?php if(($SanPhamMoiNhat['page'] == $i)){
						echo "<div class='page-link'>".$i."</div>";
					}else{
						echo "<a class='page-link' href='?controller=trangchu&action=index&page=".$i."'>".$i."</a>";
					}
				?>
			</li>
		<?php } ?>
		<li class="page-item <?php echo ($SanPhamMoiNhat['page'] == $SanPhamMoiNhat['pages'] ) ? 'hidden-xs-up':'' ?>">
			<a class="page-link" href="?controller=trangchu&active=index&page=<?php echo $SanPhamMoiNhat['page']+1; ?>" aria-label="Next">
				<span aria-hidden="true">&raquo;</span>
				<span class="sr-only">Next</span>
			</a>
		</li>
	</ul>
	</nav>
</div>