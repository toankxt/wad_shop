<?php include('header.php') ?>
<main class="main">
    <div class="frames-register">
        <div class="col-sm-6 offset-xs-3">
            <div class="jumbotron">
                <h1 class="display-3">Đăng ký tài khoản</h1>
                <hr class="my-2">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="text" name="fullname" class="form-control" id="txtFullname" required placeholder="Họ & Tên">
                        <small class="helpId" id="helpFullname" ></small>
                    </div>
                    <div class="form-group ctg">
                        <div class="ctg">
                            <input type="email" name="email" class="form-control" id="txtEmail" required placeholder="Email của bạn" onchange="checkEmailNow(this)">
                            <div class="checkicon"></div>
                        </div>
                        <small class="helpId" id="helpEmail" ></small>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" required class="form-control" id="txtPassword" placeholder="Mật khẩu">
                        <small class="helpId" id="helpPassword" ></small>
                    </div>
                    <div class="form-group">
                        <input type="password" name="reqpassword" required class="form-control" id="txtReqPassword" placeholder="Xác nhận mật khẩu">
                        <small class="helpId" id="helpRepPassword"></small>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4 col-xs-4 text-center">
                                <select id="date" required class="form-control">
                                    <option value="0">Ngày</option>
                                    <?php $date = 0; ?>
                                    <?php while($date<31){ $date++; ?>
                                        <option value="<?php echo $date; ?>"><?php echo $date; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4 col-xs-4 text-center">
                                <select id="month" required class="form-control">
                                    <option value="0">Tháng</option>
                                    <?php $month = 0; ?>
                                    <?php while($month<12){ $month++; ?>
                                        <option value="<?php echo $month; ?>">Tháng <?php echo $month;  ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4 col-xs-4 text-center">
                                <select id="year" required class="form-control">
                                    <option value="0">Năm</option>
                                    <?php $year = date("Y")+1; ?>
                                    <?php while($year>1950){ $year--; ?>
                                        <option value="<?php echo $year;?>"><?php echo $year;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <small class="helpId" id="helpRepPassword"></small>
                    </div>
                    <div class="form-group">
                        <label for="" id="title-sex">Giới tính:</label>
                        <input type="radio" value="1" name="gender" checked> <label for=""> Nam</label>
                        <input type="radio" value="0" name="gender"> <label for=""> Nữ</label>
                    </div>
                    <div id="checkResult" class="text-xs-center" style="color:green;font-family:'Times New Roman', Times, serif;"></div>
                    <div id="checkError" class="text-xs-center" style="color:red;font-family:'Times New Roman', Times, serif;"></div>
                    <div class="form-group usersuccess">
                        <a id="User_register" class="btn btn-outline-danger btn-block">Đăng ký</a>
                    </div>
                </form>
                <!-- form -->
            </div>
        </div>
    </div>
</main>
<!--Check Email Register-->
<script>
    const HTMLcheckTrue='<i class="fa fa-check" style="color:green"></i>'
    const HTMLcheckFalse='<i class="fa fa-times" style="color:red;"></i>'
    const checkEmailNow=elm=>{
        let text=elm.value;
        let elementCheck=document.querySelector(".frames-register .checkicon")
        if(!validateEmail(text)){
            return elementCheck.innerHTML=HTMLcheckFalse;
        }
        let url =`?controller=dangky&action=check&email=${text}`
        loadMethodGet(url,(res)=>{
            if(res==1){
                while(elementCheck.firstChild){
                    elementCheck.removeChild(elementCheck.firstChild)
                }
                elementCheck.innerHTML=HTMLcheckTrue;
            }else{
                elementCheck.innerHTML=HTMLcheckFalse;
            }
        })
    }
</script>
<!-- Submit Register -->
<script>
    const elmSubmit = document.getElementById('User_register'),
        helpEmail = document.getElementById('helpEmail'),
        helpFullname = document.getElementById('helpFullname'),
        helpPassword = document.getElementById('helpPassword'),
        helpRepPassword = document.getElementById('helpRepPassword'),
        checkResult = document.getElementById('checkResult'),
        checkError = document.getElementById('checkError');
    let url =`?controller=dangky&action=dangky`
    elmSubmit.addEventListener('click', e =>{
        const email = document.getElementById('txtEmail').value.trim(),
            fullname = document.getElementById('txtFullname').value.trim(),
            password = document.getElementById('txtPassword').value,
            reqpassword = document.getElementById('txtReqPassword').value,
            date = document.getElementById('date').value,
            month = document.getElementById('month').value,
            year = document.getElementById('year').value,
            genderCheck = document.getElementsByName('gender');
        let gender = 1,
            validate = true;
            birthday = null;
        if(genderCheck[1].checked) gender = 0
        // validate
        if( !validateEmail(email)){
            helpEmail.innerHTML = "Bạn vui lòng nhập đúng định dạng Email"
            validate = false
        }
        //email
        if(email === ''){
            helpEmail.innerHTML = "Bạn vui lòng nhập Email"
            validate = false
        }else{
            helpEmail.innerHTML =''
        }
        // full name
        if(fullname === ''){
            helpFullname.innerHTML = "Bạn vui lòng nhập tên"
            validate = false
        }else{
            helpFullname.innerHTML = ''
        }
        // Password
        if(password === ''){
            helpPassword.innerHTML = "Bạn vui lòng nhập mật khẩu"
            validate = false
        }else{
            helpPassword.innerHTML = ''
            if(reqpassword !== password){
                helpRepPassword.innerHTML = "Bạn vui lòng nhập khớp mật khẩu"
                validate = false
            }else{
                helpRepPassword.innerHTML = ''
            }
        }
        //Date
        if(date == 0 || year == 0 || month == 0){
            birthday = null
        }else{
            birthday = `${year}-${month}-${date}`
        }

        // if Validate => return
        if(!validate) return
        let data = {
            email,
            fullname,
            password,
            reqpassword,
            birthday,
            gender
        }
        loadDoc(url,data, res => {
            checkResult.innerHTML = ''
            checkError.innerHTML = ''
            if(res == '1'){
                // notify create account success
                checkResult.innerHTML = 'Tạo tài khoản thành công'
                // reset input
                document.getElementById('txtEmail').value = ''
                document.getElementById('txtFullname').value = ''
                document.getElementById('txtPassword').value = ''
                document.getElementById('txtReqPassword').value = ''
                document.getElementById('date').value = 0
                document.getElementById('month').value = 0
                document.getElementById('year').value = 0
                document.querySelector('.checkicon').innerHTML = ''
                document.getElementsByName('gender')[0].checked = true
            }else{
                checkError.innerHTML = 'Đăng ký thất bại'
            }
        })
        e.preventDefault();
    })
</script>
<?php include('footer.php') ?>
