<!DOCTYPE html>
<html lang="en"><head>
	<title> BTL - PHP </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="assets/vendor/bootstrap.js"></script>
 	<script type="text/javascript" src="assets/js/1.js"></script>
	<link rel="stylesheet" href="assets/vendor/bootstrap.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome.css">
 	<link rel="stylesheet" href="assets/css/1.css">
 	<link href="https://fonts.googleapis.com/css?family=Black+Ops+One&amp;subset=latin-ext" rel="stylesheet">
    <style>
        ._f_nav{
            color:#554c57;
        }
    </style>
</head>
<body>

<header id="top">
	<div class="container">
		<img src="assets/images/logo.png">
	</div>
</header> <!-- end header -->
			<!-- Mobile -->
<nav class="navbar navbar-expand-sm navbar-light bg-light Mobile">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <div class="container">
                <a class="nav-item nav-link click" href="index.php">Trang chủ</a>
                <a class="nav-item nav-link click" href="?controller=gioithieu&action=index">Giới thiệu</a>
                <a class="nav-item nav-link click" href="#">Thu mua thanh lý</a>
                <a class="nav-item nav-link click" href="#">Bảng báo giá</a>
                <a class="nav-item nav-link click" href="#">Quy định bảo hành</a>
                <a class="nav-item nav-link click" href="index.php?xem=bando">Bản đồ</a>
                <a class="nav-item nav-link click" href="#">Liên hệ</a>
                <a class="nav-item nav-link click" href="#">Hotline : 00000000000</a>
            </div>
        </div>
    </div>
</nav> <!-- end nav Mobile-->
<div class="navbar-nav desktop">
    <div class="container">
    <div class="nav">
        <li class="nav-item">
            <a class="nav-link _f_nav active" href="index.php">Trang chủ</a>
        </li>
        <li class="nav-item">
            <a class="nav-link _f_nav" href="?controller=gioithieu&action=index">Giới thiệu</a>
        </li>
        <li class="nav-item">
            <a class=" nav-link _f_nav" href="#">Thu mua thanh lý</a>
        </li>
        <li class="nav-item">
            <a class="nav-link _f_nav" href="#">Bảng báo giá</a>
        </li>
        <li class="nav-item">
            <a class="nav-link _f_nav" href="#">Quy định bảo hành</a>
        </li>
        <li class="nav-item">
            <a class="nav-link _f_nav" href="#">Liên hệ</a>
        </li>
        <?php if(isset($_SESSION['user'])){
            $admin='';
            if($_SESSION['user']->level <= 1){
                $admin = "<a class='dropdown-item' href='?controller=admin&action=index'>Quản trị</a>";
            }
            echo "<li class='nav-item dropdown'>
                <a class='nav-link _f_nav dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    Xin chào: ".$_SESSION['user']->fullname."
                </a>
                <div class='dropdown-menu' aria-labelledby='navbarDropdown'>
                    <a class='dropdown-item' href='#'>Thông tin cá nhân</a>
                    $admin
                    <a class='dropdown-item' href='?controller=trangchu&action=dangxuat'>Đăng xuất</a>
                </div>
            </li>";
        }else{
            echo "<li class='nav-item'>
                    <a class='nav-link _f_nav' href='#form-dangnhap' data-toggle='modal'>Đăng nhập</a>
                </li>";
        }?>
    </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="form-dangnhap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex;justify-content: space-between;">
                <h5 class="modal-title" id="exampleModalLabel" style="width: 100%;">Đăng Nhập</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/login" method="post">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="username" id="username-access" class="form-control" placeholder="email@gmail.com" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Mật khẩu</label>
                        <input type="password" name="password"  id="password-access" maxlength="12" class="form-control" placeholder="Nhập mật khẩu" aria-describedby="helpId">
                    </div>
                    <div id="checkErrorLogin" class="text-xs-center" style="color:red;font-family:'Times New Roman', Times, serif;"></div>
                    <div class="form-group">
                        <div id="login_now" class="btn btn-outline-success btn-block ">Đăng Nhập</div>
                        <!-- <button type="button" class="btn btn-outline-primary btn-block" id="UserAcess">Đăng Nhập</button> -->
                    </div>

                    <div class="form-group">
                        <a href="?controller=dangky" class="btn btn-outline-danger btn-block">Đăng Ký</a>
                    </div>
                    <div class="form-group text-right">
                        <a href="/forget" class="forgetPw">Quên mật khẩu</a>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- end -->
<script>
    const loadDoc=(url,data,cb)=>{
        if (window.XMLHttpRequest) {
            //code for IE7+, Firefox, Chrome and Opera
            xhr = new XMLHttpRequest();
        }else {
            //code for IE6, IE5
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                cb(this.responseText);
            }
        };
        xhr.open("POST",url,true);
        xhr.setRequestHeader("cache-control", "no-cache");
        xhr.setRequestHeader("content-type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(data));
    }
    const loadMethodGet=(url,cb)=>{
        if (window.XMLHttpRequest) {
            //code for IE7+, Firefox, Chrome and Opera
            xhr = new XMLHttpRequest();
        }else {
            //code for IE6, IE5
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                cb(this.responseText);
            }
        };
        xhr.open("GET",url,true);
        xhr.send();
    }
    const validateEmail=(email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
</script>

<script>
    const checkErrorLogin = document.getElementById("checkErrorLogin")
    const login_now = document.getElementById("login_now")
    login_now.addEventListener("click", e=>{
        const email = document.getElementById("username-access").value,
            password = document.getElementById("password-access").value;
        if(email == "" || password == ""){
            return checkError.innerHTML ="Tài khoản hoặc mật khẩu không đúng"
        }
        let data = { email, password }
        let url = "?controller=trangchu&action=dangnhap"
        loadDoc(url,data, res => {
            console.log(res);
            if(res.indexOf('1') != -1){
                location.reload()
            }else{
                checkErrorLogin.innerHTML = "Tài khoản hoặc mật khẩu không đúng"
            }
        })
        e.preventDefault();
    })
</script>