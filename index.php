<?php
    session_start();
    // Connect Database
    include "Models/DBconfig.php";
    include "utils/slug.php";
    $db = new Database;
    $db->connect();
    // check router
    // slug
    if(isset($_GET['controller'])){
        $controller = $_GET['controller'];
    }else{
        $controller = '';
    }
    switch($controller){
        case 'trangchu':{
            require_once('controller/TrangChu/index.php');
            break;
        }
        case 'chitiet':{
            require_once('controller/ChiTiet/index.php');
            break;
        }
        case 'gioithieu':{
            require_once('controller/GioiThieu/index.php');
            break;
        }
        case 'dangky':{
            require_once('controller/DangKy/index.php');
            break;
        }
        case 'giohang':{
            require_once('controller/GioHang/index.php');
            break;
        }
        case 'muahang':{
            require_once('controller/MuaHang/index.php');
            break;
        }
        case 'admin':{
            if(!isset($_SESSION['user']) || $_SESSION['user']->level >1){
                return header('location:?controller=trangchu&action=index');
            }
            require_once('controller/Admin/index.php');
            break;
        }
        default:{
            require_once('controller/TrangChu/index.php');
            break;
        }
    }
?>