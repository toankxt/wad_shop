
<?php 
    function LayDanhMuc($db){
        $tblTable = "category";
        return $db->getAllData($tblTable);
    }
    function select_limit($limit){
        $limit = (int) isset($_GET['limit']) ? $_GET['limit'] : $limit ; // giới hạn mấy s/p
        return $limit == 0 ? 10 : $limit;
    }
    function select_page($page){
        $page = (int) isset($_GET['page']) ? $_GET['page'] : $page ; // giới hạn mấy s/p
        return $page == 0 ? 1 : $page;
    }
    function select_sort($sort){
        $sort = isset($_GET['sort']) ? $_GET['sort'] : $sort ; // sắp xếp
        // 1 giảm dần (desc) - còn lại tăng dần (asc)
        $RsSort ='id desc';
        if($sort != 1){
            $RsSort = '';
            try{
                // substr(string,start,end)
                // strpos(stirng,regex)
                $sort = substr($sort,strpos($sort,'(')+1 , strpos($sort,')')-1);
                $sort = explode(',',$sort);
                foreach($sort as $value){
                    $value = explode('=',$value);
                    $typeSort = (int) $value[1]; // đổi kiểu. k phải kiểu số sẽ là 0;
                    if(strpos($RsSort,$value[0]) === FALSE){ //  check xem săp xếp trường này đã có chưa nếu có rồi thi thôi
                        $field = $typeSort > 0 ? "$value[0] asc" : "$value[0] desc";
                        $RsSort = $RsSort == '' ? $field : "$RsSort,$field";
                    }
                }
            }catch(Exception $ex){
                $RsSort = 'id desc';
            }
        }
        return $RsSort;
    }
    function MultiQuery($db,$tblTable,$limit = 10 ,$page = 1,$sort = 1, $condition=null){
        //limit
        $limit = select_limit($limit);
        // check page part 1
        $page = select_page($page);
        // Xử lý sắp xếp
        $sort = select_sort($sort);
        // Lấy tổng số sản phẩm
        $count = $db->count_document($tblTable,$condition);
        // Trường hợp lấy tất cả bản ghi tức limit=max => lấy tất cả
        $limit = $limit == 'max' ? $count : $limit;
        $pages = ceil($count/$limit);
        // Check page part 2
        $page = $page > $pages ? $pages : $page;
        //  vị trí lấy s/p
        $offset = $limit * ($page - 1);
        // Lấy dữ liệu theo yêu cầu
        $items = $db->queryData($tblTable,$limit,$offset,$sort, $condition);
        // trả về kết quả
        $result = array();
        $result['total'] = $count; // tổng số sản phẩm
        $result['page'] = $page; // page hiện tại
        $result['pages'] = $pages; // tổng số page
        $result['limit'] = $limit; // giới hạn bài viết
        $result['items'] = $items; // danh sách sản phẩm
        return $result;
    }
?>